## Regular Expression Engine
Description: <br>
This project uses Ocaml(A Function Programming Language) to implement algorithms to see whether a string is matched by a NFA. It has functions to convert an NFA to a DFA using subset construction and to convert a regular expression to an NFA. 
NFAs and DFAs are implemented in nfa.ml, and regexes are implemented in regex.ml.
