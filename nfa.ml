open List
open Sets
(*********)
(* Types *)
(*********)

type ('q, 's) transition = 'q * 's option * 'q

type ('q, 's) nfa_t = {
  sigma: 's list;
  qs: 'q list;
  q0: 'q;
  fs: 'q list;
  delta: ('q, 's) transition list;
}

(***********)
(* Utility *)
(***********)

(* explode converts a string to a character list *)
let explode (s: string) : char list =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l)
  in
  exp (String.length s - 1) []

(****************)
(* NFAs *)
(****************)


let rec fold f a xs = match xs with
| [] -> a
| x :: xt -> fold f (f a x) xt

let move (nfa: ('q,'s) nfa_t) (qs: 'q list) (s: 's option) : 'q list =
    if fold (fun a b -> if Some b = s then true else a) false nfa.sigma = false then [] 
    else let return_val = [] in fold (fun a b -> insert_all (fold(fun c (x,y,z) -> if b = x then if s = y then z::c else c else c ) [] nfa.delta) a) return_val qs

let rec e_closure_helper (nfa: ('q,'s) nfa_t) states_left states_visited : 'q list = 
  match states_left with
  |[] -> []
  |x :: xs -> x :: e_closure_helper nfa (insert_all (fold (fun c (a,y,z) -> if a = x then if y = None then if elem z states_visited then c else (insert z c) else c else c ) [] nfa.delta) xs) (x::states_visited)

let e_closure (nfa: ('q,'s) nfa_t) (qs: 'q list) : 'q list =
  e_closure_helper nfa qs [];;


let rec accept_helper nfa idx cur_string = 
  match cur_string with 
  | [] -> let temp = (e_closure nfa idx) in 
    if (fold(fun a b-> if List.mem b nfa.fs then true else a) false temp) then true else false
  | x::xs -> accept_helper nfa (move nfa (e_closure nfa idx) (Some x)) xs


let accept (nfa: ('q,char) nfa_t) (s: string) : bool =
  accept_helper nfa ([nfa.q0]) (explode s)

(*******************************)
(* Subset Construction *)
(*******************************)

let rec helper nfa alphabet idx = 
  match alphabet with 
  |[] -> []
  |x::xs -> if (move nfa (e_closure nfa idx) (Some x)) <> [] then 
    ((e_closure nfa idx), (Some x), e_closure nfa (move nfa (e_closure nfa idx) (Some x)))::helper nfa xs idx
  else 
    helper nfa xs idx

let rec new_states_match nfa alphabet idx= 
  match alphabet with 
  | [] -> []
  | x::xs -> e_closure nfa (move nfa (e_closure nfa idx) (Some x))::new_states_match nfa xs idx

let rec fsl_helper nfa visited n_visited = 
  match n_visited with 
  | [] -> visited
  | x::xs -> if (List.mem x visited) = true then 
  fsl_helper nfa visited xs 
  else let cur_n_visited = new_states_match nfa nfa.sigma x in 
  fsl_helper nfa (visited@[x]) (xs@cur_n_visited)

let new_states (nfa: ('q,'s) nfa_t) (qs: 'q list) : 'q list list =
  let states = fold(fun a b -> (move nfa qs (Some b)) :: a) [] nfa.sigma in map(fun c -> e_closure nfa c) states


let rec transitions nfa idx = 
  match idx with 
  | [] -> []
  | x::xs -> helper nfa nfa.sigma x@transitions nfa xs




let rec trans_aux nfa n_visited = 
  match n_visited with 
  | [] -> []
  | x::xs -> transitions nfa x@(trans_aux nfa xs)


let new_trans (nfa: ('q,'s) nfa_t) (qs: 'q list) : ('q list, 's) transition list =
  List.rev (fold (fun a b -> (qs, Some b, e_closure nfa (move nfa qs (Some b)))::a) [] nfa.sigma)


let rec ef_states nfa sts = 
  match sts with 
  | [] -> []
  | x::xs -> if (fold(fun a b-> if List.mem b x then true else a) false nfa.fs) then 
    x::ef_states nfa xs
  else ef_states nfa xs


let new_finals (nfa: ('q,'s) nfa_t) (qs: 'q list) : 'q list list =
  let bool = fold(fun a b -> if elem b qs then true else a) false nfa.fs in if bool = true then [qs] else []


let rec nfa_to_dfa_step (nfa: ('q,'s) nfa_t) (dfa: ('q list, 's) nfa_t)
    (work: 'q list list) : ('q list, 's) nfa_t =
    failwith "unimplemented"
 
    
let nfa_to_dfa (nfa: ('q,'s) nfa_t) : ('q list, 's) nfa_t =
  let r0 = e_closure nfa [nfa.q0] in 
  let state_set = fsl_helper nfa [] [r0] in 
  let trans = trans_aux nfa [state_set] in 
  let acc_states = ef_states nfa state_set in 
  let dfa = {qs = state_set; sigma = nfa.sigma; delta = trans; q0 = r0; fs = acc_states} in 
    dfa
